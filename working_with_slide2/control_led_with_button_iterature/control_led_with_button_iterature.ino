const int led1 = 7, led2 = 8, led3 = 10, button = 4;
unsigned long lastTime = 0;
bool lastState = false;
bool isReverse = false;

void setup() {
  pinMode(led1, OUTPUT);
  pinMode(led2, OUTPUT);
  pinMode(led3, OUTPUT);
  pinMode(button, INPUT_PULLUP);

  digitalWrite(led1, LOW);
  digitalWrite(led2, LOW);
  digitalWrite(led3, LOW);

  Serial.begin(9600);
}

void loop() {
  bool currentState = digitalRead(button) == LOW;
  if (!lastState && currentState) {
    Serial.println("----------- REVERSE -----------------");
    isReverse = !isReverse;
    delay(500);
  }
  lastState = currentState;

  if (!isReverse) {
    lighting();
  } else {
    reverseLighting();
  }
}

void lighting() {
  unsigned long currentTime = millis(),
                deltaTime = currentTime - lastTime;
  if (deltaTime < 100) {
    digitalWrite(led1, HIGH);
  } else if (deltaTime < 200) {
    digitalWrite(led2, HIGH);
  } else if (deltaTime < 800) {
    digitalWrite(led3, HIGH);
  }

  else if (deltaTime < 900) {
    digitalWrite(led1, LOW);
  } else if (deltaTime < 1000) {
    digitalWrite(led2, LOW);
  } else if (deltaTime < 2000) {
    digitalWrite(led3, LOW);
  } else {
    lastTime = currentTime;
  }
}

void reverseLighting() {
  unsigned long currentTime = millis(),
                deltaTime = currentTime - lastTime;
  if (deltaTime < 100) {
    digitalWrite(led3, HIGH);
  } else if (deltaTime < 200) {
    digitalWrite(led2, HIGH);
  } else if (deltaTime < 800) {
    digitalWrite(led1, HIGH);
  }

  else if (deltaTime < 900) {
    digitalWrite(led3, LOW);
  } else if (deltaTime < 1000) {
    digitalWrite(led2, LOW);
  } else if (deltaTime < 2000) {
    digitalWrite(led1, LOW);
  } else {
    lastTime = currentTime;
  }
}

