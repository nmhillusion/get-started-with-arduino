const int linePin[] = {2, 3, 4, 5, 6};

void setup() {
  for(int i = 0; i < 5; ++i){
    pinMode(linePin[i], INPUT);
  }

  Serial.begin(9600);
  Serial.println("starting test line 5 led...");
}

void loop() {
  for(int i = 0; i < 5; ++i){
    Serial.print("sign ");
    Serial.print(i+1);
    Serial.print(": ");
    Serial.println(digitalRead(linePin[i]));
  }
  delay(2000);
}
