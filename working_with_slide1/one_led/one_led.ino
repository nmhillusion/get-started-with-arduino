
const int LIGHT = 12;
bool ledState = false;

unsigned long previousTime = 0,
              interval = 1500;

void setup() {
  pinMode(LIGHT, OUTPUT);
  digitalWrite(LIGHT, LOW);
  ledState = false;
}

void loop() {
  unsigned long currentTime = millis();
  if(currentTime - previousTime >= interval){
    previousTime = currentTime;

    if(ledState){
      digitalWrite(LIGHT, LOW);
    }else{
      digitalWrite(LIGHT, HIGH);
    }

    ledState = !ledState;
  }
}
