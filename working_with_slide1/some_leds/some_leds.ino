const int redLed = 2, yellowLed = 8, greenLed = 7, RED_MODE = 1, YELLOW_MODE = 2, GREEN_MODE = 3;
int mode = 1;

void setup() {
  pinMode(redLed, OUTPUT);
  pinMode(yellowLed, OUTPUT);
  pinMode(greenLed, OUTPUT);

  Serial.begin(9600);
}

void loop() {
  if(RED_MODE == mode){
    lightAndWait(redLed, 10);
  }else if(YELLOW_MODE == mode){
    lightAndWait(yellowLed, 3);
  }else if(GREEN_MODE == mode){
    lightAndWait(greenLed, 10);
  }
  mode = mode + 1 == 4 ? 1 : mode + 1;
}

void lightAndWait(int led, int counter){
  switch(led){
    case redLed:
      Serial.println(":: Đèn đỏ ::");
      break;
    case yellowLed:
      Serial.println(":: Đèn vàng ::");
      break;
    case greenLed:
      Serial.println(":: Đèn xanh ::");
      break;
  }
  
  do{
    Serial.println(counter);
    digitalWrite(led, HIGH);
    delay(1000);
  }while(--counter > 0);

  digitalWrite(led, LOW);
}

