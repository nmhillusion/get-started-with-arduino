// Gọi thư viện DHT11
#include "DHT.h"            
 
#define DHTPIN 4       //Đọc dữ liệu từ DHT11 ở chân 2 trên mạch Arduino
const int DHTTYPE = DHT11;  //Khai báo loại cảm biến, có 2 loại là DHT11 và DHT22
const int led = 13;
 
DHT dht(DHTPIN, DHTTYPE);
 
void setup() {
  Serial.begin(9600);
  dht.begin();         // Khởi động cảm biến
  pinMode(led, OUTPUT);
}
 
void loop() {
  float h = dht.readHumidity();    //Đọc độ ẩm
  float t = dht.readTemperature(); //Đọc nhiệt độ
 
  Serial.print("Nhiệt độ: ");
  Serial.println(t);               //Xuất nhiệt độ
  Serial.print("Độ ẩm: ");
  Serial.println(h);               //Xuất độ ẩm
  
  Serial.println();                //Xuống hàng
  digitalWrite(led, HIGH);
  delay(1500);                     //Đợi 1 giây
  digitalWrite(led, LOW);
}
