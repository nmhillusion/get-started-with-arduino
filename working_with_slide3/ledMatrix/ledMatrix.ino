#include "LedControl.h"// thêm thư viện
#include "characters.h";
LedControl matrix = LedControl(12, 11, 10, 1);
// Chân 12 nối với chân DataIn
// Chân 11 nối với chân CLK
// Chân 10 nối với chân LOAD
// Sử dụng 1 IC MAX7219
byte* tempMatrix;
int i, j, times = 0, posStop = 1, posRun = 0;
bool paying = false;

void setup() {
  matrix.shutdown(0, false); // Bật hiển thị
  matrix.setIntensity(0, 10); // Đặt độ sáng lớn nhất
  matrix.clearDisplay(0); // Tắt tất cả led
  Serial.begin(9600);
}

void loop() {
  tempMatrix = !paying ? getMatrixCharacter('-') : getMatrixCharacter('.');

  matrix.clearDisplay(0);
  for (j = 0; j < 8; j++) {
    matrix.setRow(0, j, tempMatrix[j]);
  }

  free(tempMatrix);
  if (!paying) {
    animationStop();
  } else {
    animationRun();
  }
  delay(100);

  //  next
  paying = ++times >= 100 ? true : false;
}

void animationStop() {
  switch (posStop++) {
    case 1:
      matrix.setLed(0, 3, 0, true);
      matrix.setLed(0, 4, 0, true);
      break;
    case 2:
      matrix.setLed(0, 0, 3, true);
      matrix.setLed(0, 0, 4, true);
      break;
    case 3:
      matrix.setLed(0, 3, 7, true);
      matrix.setLed(0, 4, 7, true);
      break;
    case 4:
      matrix.setLed(0, 7, 3, true);
      matrix.setLed(0, 7, 4, true);
      break;
  }
  if (posStop == 5) posStop = 1;
}

void animationRun() {
  if (++posRun > 7) {
    posRun = -1;
  } else {
    matrix.setLed(0, 3, posRun, false);
    matrix.setLed(0, 4, posRun, false);
  }
}
