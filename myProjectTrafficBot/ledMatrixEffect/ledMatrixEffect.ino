#include "LedControl.h";// thêm thư viện cho matrix led
#include "characters.h";
#include <IRremote.h> // thư viện hỗ trợ IR remote

LedControl matrix = LedControl(4, 2, 3, 1);
// Chân 4 nối với chân DataIn
// Chân 3 nối với chân CLK
// Chân 2 nối với chân LOAD
// Sử dụng 1 IC MAX7219
const int receiverPin = 8; // chân digital 8 dùng để đọc tín hiệu
IRrecv irrecv(receiverPin); // tạo đối tượng IRrecv mới
decode_results results;// lưu giữ kết quả giải mã tín hiệu
String val2Display = "&";
int pos = 0, indexChar = 0;

void setup() {
  matrix.shutdown(0, false); // Bật hiển thị
  matrix.setIntensity(0, 10); // Đặt độ sáng lớn nhất
  matrix.clearDisplay(0); // Tắt tất cả led
  Serial.begin(9600);

  irrecv.enableIRIn(); // start the IR receiver

  byte x = B10001010, y = B11001100,
        z = (x << 5) | (y >> 4);
  Serial.println(x, BIN);
  Serial.println(y, BIN);
  Serial.println(z, BIN);
}

void loop() {
  displayMatrix();

  listenerBluetooth();
  listenerIR();
}

void listenerIR() {
  if (irrecv.decode(&results)) // nếu nhận được tín hiệu
  {
    char* val = (char*) malloc(16);
    itoa(results.value, val, 16);
    String tempStr = switchIR2String(val); // in ra Serial Monitor
    if (tempStr.compareTo("--") != 0) {
      val2Display = tempStr;
      indexChar = 0;
    }

    delay(200);
    irrecv.resume(); // nhận giá trị tiếp theo
    free(val);

    Serial.println(val2Display);
  }
}

String switchIR2String(char* val) {
  if (strcmp(val, "30cf") == 0) {
    return "1";
  } else if (strcmp(val, "18e7") == 0) {
    return "2";
  } else if (strcmp(val, "7a85") == 0) {
    return "3";
  } else if (strcmp(val, "10ef") == 0) {
    return "4";
  } else if (strcmp(val, "38c7") == 0) {
    return "5";
  } else if (strcmp(val, "5aa5") == 0) {
    return "6";
  } else if (strcmp(val, "42bd") == 0) {
    return "7";
  } else if (strcmp(val, "4ab5") == 0) {
    return "8";
  } else if (strcmp(val, "52ad") == 0) {
    return "9";
  } else if (strcmp(val, "6897") == 0) {
    return "0";
  }
  return "--";
}

void listenerBluetooth() {
  if (Serial.available() > 0) {
    String data = Serial.readString();        //Read the incoming data & store into data
    Serial.print(data);          //Print Value inside data in Serial monitor
    Serial.print("\n");
    val2Display = data;
    indexChar = 0;
  }
}

void displayMatrix() {
  byte* tempMatrix = getMatrixCharacter(val2Display.charAt(indexChar)), *nextMatrix;
  unsigned long finalMatrix;

  matrix.clearDisplay(0);

  if(indexChar + 1 < val2Display.length()){
    nextMatrix = getMatrixCharacter(val2Display.charAt(indexChar + 1));
    for (int j = 0; j < 8; j++) {
      finalMatrix = tempMatrix[j] << pos | (unsigned int)nextMatrix[j] >> (8 - pos);
//      matrix.setRow(0, j, finalMatrix);
      Serial.println(finalMatrix, BIN);

      free(finalMatrix);
    }
    free(nextMatrix);
  }else{
    for (int j = 0; j < 8; j++) {
      matrix.setRow(0, j, pos > 0 ? tempMatrix[j] << pos : tempMatrix[j] >> -pos);
    }
  }
  
  free(tempMatrix);

  delay(200);
  if (++pos > 7) {
    pos = 0;
    if (++indexChar >= val2Display.length()) {
      indexChar = 0;
    }
  }
}
