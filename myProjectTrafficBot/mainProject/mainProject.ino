#include <Servo.h>
#include "LedControl.h";// thêm thư viện cho matrix led
#include "characters.h";
#include <IRremote.h> // thư viện hỗ trợ IR remote

Servo myServo;
const int servoPin = 5, interval = 100;
LedControl matrix = LedControl(4, 2, 3, 1);
// Chân 4 nối với chân DataIn
// Chân 3 nối với chân CLK
// Chân 2 nối với chân LOAD
// Sử dụng 1 IC MAX7219
bool paying = false;
unsigned long lastTime = 0;
const int receiverPin = 8; // chân digital 8 dùng để đọc tín hiệu
IRrecv irrecv(receiverPin); // tạo đối tượng IRrecv mới
decode_results results;// lưu giữ kết quả giải mã tín hiệu

void setup() {
  matrix.shutdown(0, false); // Bật hiển thị
  matrix.setIntensity(0, 10); // Đặt độ sáng lớn nhất
  matrix.clearDisplay(0); // Tắt tất cả led
  Serial.begin(9600);
  myServo.attach(servoPin);

  lastTime = millis();
  myServo.write(175);

  irrecv.enableIRIn(); // start the IR receiver
}

void loop() {
  displayMatrix();

  listenerBluetooth();

  listenerIR();

  navigationServo();
}

bool lastStatus = false;
void navigationServo() {
  if (lastStatus != paying) {
    if (!paying) {
      myServo.write(90);
    } else {
      myServo.write(0);
    }

    lastStatus = paying;
  }
}

void listenerIR(){
  if (irrecv.decode(&results)) // nếu nhận được tín hiệu
  {
    char* val = (char*) malloc(16);
    itoa(results.value, val, 16);
    Serial.println(switchIR2String(val)); // in ra Serial Monitor
    delay(200);
    irrecv.resume(); // nhận giá trị tiếp theo
    free(val);
  }
}

String switchIR2String(char* val){
  if(strcmp(val, "30cf") == 0){
    paying = true;
    return "1";
  }else if(strcmp(val, "18e7") == 0){
    return "2";
  }else if(strcmp(val, "7a85") == 0){
    return "3";
  }else if(strcmp(val, "10ef") == 0){
    return "4";
  }else if(strcmp(val, "38c7") == 0){
    return "5";
  }else if(strcmp(val, "5aa5") == 0){
    return "6";
  }else if(strcmp(val, "42bd") == 0){
    return "7";
  }else if(strcmp(val, "4ab5") == 0){
    return "8";
  }else if(strcmp(val, "52ad") == 0){
    return "9";
  }else if(strcmp(val, "6897") == 0){
    paying = false;
    return "0";
  }
  return "--";
}

void listenerBluetooth() {
  if (Serial.available() > 0) {
    char data = Serial.read();        //Read the incoming data & store into data
    Serial.print(data);          //Print Value inside data in Serial monitor
    Serial.print("\n");
    if (data == '1') {           // Checks whether value of data is equal to 1
      paying = true;
    } else if (data == '0') {       //  Checks whether value of data is equal to 0
      paying = false;
    }
  }
}

void displayMatrixOrigin() {
  byte* tempMatrix = !paying ? getMatrixCharacter('-') : getMatrixCharacter('.');
  matrix.clearDisplay(0);
  for (int j = 0; j < 8; j++) {
    matrix.setRow(0, j, tempMatrix[j]);
  }
  free(tempMatrix);
}

void displayMatrix() {
  unsigned long currentTime = millis();
  if (currentTime - lastTime > interval) {
    lastTime = currentTime;


    displayMatrixOrigin();
    if (!paying) {
      animationStop(matrix);
    } else {
      animationRun(matrix);
    }
  }
}
