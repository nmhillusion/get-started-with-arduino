#include <Servo.h>

int servoPin = 8, angle = 0;
Servo myServo;

void setup() {
  myServo.attach(servoPin);

  Serial.begin(9600);
}

void loop() {
  angle = angle == 90 ? 0 : 90;
  myServo.write(angle);

  delay(1000);
}
