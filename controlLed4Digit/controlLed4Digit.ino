#include "SevenSegmentTM1637.h"

/* initialize global TM1637 Display object
*  The constructor takes two arguments, the number of the clock pin and the digital output pin:
* SevenSegmentTM1637(byte pinCLK, byte pinDIO);
*/
const byte PIN_CLK = 8;   // define CLK pin (any digital pin)
const byte PIN_DIO = 7;   // define DIO pin (any digital pin)
SevenSegmentTM1637    display(PIN_CLK, PIN_DIO);

void led4digit(){
  display.begin();            // initializes the display
  display.setBacklight(100);  // set the brightness to 100 %
  for(int i = 0; i < 10; ++i){
    if(i%2 == 0){
      display.print("  U ");
    }else{
      display.print("    ");
    }

    delay(500);
  }

  for(int i = 0; i < 6; ++i){
    if(i%2 == 0){
      display.print("  UN");
    }else{
      display.print("  U ");
    }

    delay(500);
  }

  display.print("  UN");
}

// run setup code
void setup() {
  Serial.begin(9600);         // initializes the Serial connection @ 9600 baud
  led4digit();
}

// run loop (forever)
void loop() {
  if(Serial.available()){
    String value = Serial.readString();
    display.print(value);
    Serial.print("Arduino reveived: ");
    Serial.println(value);

    Serial.println("Arduino say hello to you!!!");
  }
}
