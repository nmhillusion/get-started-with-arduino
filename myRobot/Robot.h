class Robot{
    public:
        Robot();
        void setup();
        void setPulseHead(bool value);
        void setFace(String face);
        void setLeftArm(int degree);
        void setRightArm(int degree);
        void setWeapon(int value);
        void loop();
    private:
        void actionHead();
};
