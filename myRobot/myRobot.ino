#include <ESP8266WiFi.h>
#include "ESP8266HTTPClient.h"
#include "Robot.h"

const char* ssid = "LTT";
const char* password = "system1234";

Robot myRobot;
HTTPClient http;  //Declare an object of class HTTPClient
const int interval = 5000;
unsigned long lastTime = 0;

//void getPulseHead();
//void getFace();
//void getLeftArm();
//void getRightArm();
//void getClean();

void setup() {
  Serial.begin(9600);
  Serial.println("--- > setting wifi...");
  WiFi.begin(ssid, password);
  delay(5000);
  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("Connecting..");
  }
  Serial.println("========= STARTING ROBOT ==========");
  myRobot.setup();

  getPulseHead();
  getFace();
  getLeftArm();
  getRightArm();
  getClean();
  Serial.println(" finish setup...");
}

void loop() {
  myRobot.loop();

  unsigned long currentTime = millis();

  if (currentTime - lastTime >= interval && WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
    Serial.println(" :: get status from server :: ");
    lastTime = currentTime;
    getPulseHead();
    getFace();
    getLeftArm();
    getRightArm();
    getClean();
  }
}

void getPulseHead() {
  http.begin("http://nmhillusion.herokuapp.com/arduino/controlRobot/getStatus/pulseHead");  //Specify request destination
  int httpCode = http.GET();                                                                  //Send the request

  if (httpCode > 0) { //Check the returning code

    String payload = http.getString();   //Get the request response payload
    Serial.println("pulse head: " + payload);                     //Print the response payload
    myRobot.setPulseHead(payload.equals("true") ? true : false);
  }

  http.end();   //Close connection
}
void getFace() {
  http.begin("http://nmhillusion.herokuapp.com/arduino/controlRobot/getStatus/face");  //Specify request destination
  int httpCode = http.GET();                                                                  //Send the request

  if (httpCode > 0) { //Check the returning code

    String payload = http.getString();   //Get the request response payload
    Serial.println("face: " + payload);                     //Print the response payload
    myRobot.setFace(payload);
  }

  http.end();   //Close connection
}
void getLeftArm() {
  http.begin("http://nmhillusion.herokuapp.com/arduino/controlRobot/getStatus/leftArm");  //Specify request destination
  int httpCode = http.GET();                                                                  //Send the request

  if (httpCode > 0) { //Check the returning code

    String payload = http.getString();   //Get the request response payload
    Serial.println("left arm" + payload);                     //Print the response payload
    myRobot.setLeftArm(payload.toInt());
  }

  http.end();   //Close connection
}
void getRightArm() {
  http.begin("http://nmhillusion.herokuapp.com/arduino/controlRobot/getStatus/rightArm");  //Specify request destination
  int httpCode = http.GET();                                                                  //Send the request

  if (httpCode > 0) { //Check the returning code

    String payload = http.getString();   //Get the request response payload
    Serial.println("right arm: " + payload);                     //Print the response payload
    myRobot.setRightArm(payload.toInt());
  }

  http.end();   //Close connection
}
void getClean() {
  http.begin("http://nmhillusion.herokuapp.com/arduino/controlRobot/getStatus/clean");  //Specify request destination
  int httpCode = http.GET();                                                                  //Send the request

  if (httpCode > 0) { //Check the returning code

    String payload = http.getString();   //Get the request response payload
    Serial.println("clean: " + payload);                     //Print the response payload
    myRobot.setWeapon(payload.equals("true") ? HIGH : LOW);
  }

  http.end();   //Close connection
}
