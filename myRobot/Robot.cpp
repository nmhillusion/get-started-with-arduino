#include <Arduino.h>
#include <Servo.h>
#include "LedControl.h"
#include "Robot.h"

const int pinHead = 4,  //  D2
          pinMatrix[3] = {12, 13, 15},  //  {D 6 -> 8}
                         pinLeftArm = 0, //  D3
                         pinRightArm = 5,  //  D1
                         pinMotor = 16;  //  D0
bool isPulseHead = false;
int currentHeadAngle = 45,
    stepMotor = 2;

byte* symbolFun = new byte[8] { 0x3c, 0x42, 0xa5, 0x81, 0xa5, 0x99, 0x42, 0x3c},
*symbolSad = new byte[8] { 0x3c, 0x42, 0xa5, 0x81, 0x99, 0xa5, 0x42, 0x3c},
*symbolBlink = new byte[8] { 0x3c, 0x42, 0xa5, 0x81, 0xbd, 0x81, 0x42, 0x3c};

LedControl matrix = LedControl(pinMatrix[0], pinMatrix[2], pinMatrix[1]);
Servo headServo, leftArmServo, rightArmServo;

Robot::Robot() {}

void Robot::setup() {
  pinMode(pinMotor, OUTPUT);

  headServo.attach(pinHead);
  leftArmServo.attach(pinLeftArm);
  rightArmServo.attach(pinRightArm);

  // TODO: init servo

  matrix.shutdown(0, false); // Bật hiển thị
  matrix.setIntensity(0, 5); // Đặt độ sáng lớn nhất
  matrix.clearDisplay(0); // Tắt tất cả led
  setFace("fun");
}

void Robot::loop() {
  actionHead();
}

void Robot::setPulseHead(bool value) {
  isPulseHead = value;
}

void Robot::actionHead() {
  if (isPulseHead) {
    if (currentHeadAngle <= 30 || currentHeadAngle >= 60) {
      stepMotor = -stepMotor;
    }
    currentHeadAngle += stepMotor;
    headServo.write(currentHeadAngle);
  }
}

void Robot::setFace(String face) {
  byte* tempMatrix;
  if (face.equals("fun")) {
    for (int i = 0; i < 8; i++) {
      matrix.setRow(0, i, symbolFun[i]);
    }
  } else if (face.equals("sad")) {
    for (int i = 0; i < 8; i++) {
      matrix.setRow(0, i, symbolSad[i]);
    }
  } else if (face.equals("blink")) {
    for (int i = 0; i < 8; i++) {
      matrix.setRow(0, i, symbolBlink[i]);
    }
  } else {
    return;
  }
  //  matrix.clearDisplay(0);
}

void Robot::setLeftArm(int degree) {
  leftArmServo.write(degree);
}

void Robot::setRightArm(int degree) {
  rightArmServo.write(degree);
}

int cleanStatus = 0;
void Robot::setWeapon(int value) {
  if (value != cleanStatus) {
    digitalWrite(pinMotor, value);
    cleanStatus = value;
  }
}
